import logo from './logo.svg';
import './App.css';
import {Button,Form,Col} from 'react-bootstrap';
import {useState,useEffect} from 'react';
import Axios from 'axios';

function App() {
  const[nom ,setNom]=useState("");
  const[email ,setEmail]=useState("");
  const[password ,setPassword]=useState("");
  useEffect(()=>{
   // Axios.get('http://localhost:3008/get').then((response)=>{console.log(response)})
  },[]

  )
  const display=()=>{
    Axios.get('http://localhost:3008/display').then(()=>{
      alert("successful display");
    })
  }
 const  SubmitValue=()=>{
  Axios.post('http://localhost:3008/insert',{nom:nom,email:email,password:password}).then(()=>{
    alert("successful insert ");
  })
  }
  const  LoginValue=()=>{
    Axios.post('http://localhost:3008/login',{nom:nom,email:email,password:password}).then(()=>{
      alert("successful login ");
  
    })
    }

  return (
    <div className="App">

<Form>
<Form.Group controlId="formBasicEmail">
    <Form.Label>Name </Form.Label>
    <Form.Control type="text" placeholder="Enter name"  onChange={(e)=>{setNom(e.target.value)}}/>
   
  </Form.Group>
  <Form.Group controlId="formBasicEmail">
    <Form.Label>Email address</Form.Label>
    <Form.Control type="email" placeholder="Enter email" onChange={(e)=>{setEmail(e.target.value)}} />
   
  </Form.Group>

  <Form.Group controlId="formBasicPassword">
    <Form.Label>Password</Form.Label>
    <Form.Control type="password" placeholder="Password" onChange={(e)=>{setPassword(e.target.value)}}/>
  </Form.Group>
  <Form.Row>
    <Col>
    <Button variant="primary" type="submit" onClick={SubmitValue}>
    Submit
  </Button>
    </Col>
    <Col>
    <Button variant="primary" type="submit" onClick={LoginValue}>
   Login
  </Button>
    </Col>
    <Col>
    
<a href="/auth"> display </a>
    </Col>
  </Form.Row>
 
 
  
</Form>
   
    </div>
  );
}

export default App;
